from Modelos.Inscripcion import Inscripcion

class ControladorInscripcion():

    def init(self):
        print("Creando ControladorInscripcion")

    def index(self):
        print("Listar todas las inscripcion")
        unInscripcion = {
            "_id": "cde678",
            "año": "2019",
            "semestre": "Noveno",
            "nota_final": "5.0"
        }
        return [unInscripcion]

    def create(self, infoInscripcion):
        print("Crear una inscripcion")
        elInscripcion = Inscripcion(infoInscripcion)
        return elInscripcion._dict_

    def show(self, id):
        print("Mostrando una inscripcion con id ", id)
        elInscripcion = {
            "_id": id,
            "año": "2019",
            "semestre": "Noveno",
            "nota_final": "5.0"
        }
        return elInscripcion

    def update(self, id, infoInscripcion):
        print("Actualizando de una inscripcion por id", id)
        elInscripcion = Inscripcion(infoInscripcion)
        return elInscripcion._dict_

    def delete(self, id):
        print("Elimiando una inscripcion por id ", id)
        return {"deleted_count":1}