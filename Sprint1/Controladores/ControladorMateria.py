from Modelos.Materia import Materia

class ControladorMateria():

    def init(self):
        print("Creando ControladorMateria")

    def index(self):
        print("Listar todas las materias")
        unMateria = {
            "_id": "abc456",
            "nombre": "Español"
        }
        return [unMateria]

    def create(self, infoMateria):
        print("Crear una materia")
        elMateria = Materia(infoMateria)
        return elMateria._dict_

    def show(self, id):
        print("Mostrando una materia con id ", id)
        elMateria = {
            "_id": id,
            "nombre": "Español"
        }
        return elMateria

    def update(self, id, infoMateria):
        print("Actualizando materia con id", id)
        elMateria = Materia(infoMateria)
        return elMateria._dict_

    def delete(self, id):
        print("Elimiando materia con id ", id)
        return {"deleted_count": 1}