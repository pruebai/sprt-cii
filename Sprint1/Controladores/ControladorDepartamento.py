from Modelos.Departamento import Departamento

class ControladorDepartamento():

    def init(self):
        print("Creando ControladorDepartamento")

    def index(self):
        print("Listar todos los departamento")
        unDepartamento = {
            "_id": "cde123",
            "nombre": "Cauca",
        }
        return [unDepartamento]

    def create(self, infoDepartamento):
        print("Crear un departamento")
        elDepartamento = Departamento(infoDepartamento)
        return elDepartamento.__dict__

    def show(self, id):
        print("Mostrando un departamento con id ", id)
        elDepartamento = {
            "_id": id,
            "nombre": "Cauca"
        }
        return elDepartamento

    def update(self, id, infoDepartamento):
        print("Actualizando departamento por id", id)
        elDepartamento = Departamento(infoDepartamento)
        return elDepartamento.__dict__

    def delete(self, id):
        print("Elimiando departamento por id ", id)
        return {"deleted_count": 1}